rgarrigue.workstation
=====================

Ubuntu LTS workstation installation, with Atom editor, Guake terminal, AWS cli & Awless, Terraform & Terragrunt, etc. [![Build Status](https://travis-ci.org/rgarrigue/ansible-role-workstation.svg?branch=master)](https://travis-ci.org/rgarrigue/ansible-role-workstation)

TODO :
- get back from git log vpn & behind a conditional too  
- move browser related stuff in a browser.yml ?
- move gnome related stuff in a gnome.yml and install https://extensions.gnome.org/extension/906/sound-output-device-chooser/

Requirements
------------

This role will need superuser / root access for packages installation. It's only meant and tested for Ubuntu LTS editions, should work on non-LTS, might work on apt-based distributions like Debian and Linux Mint.
See `meta/main.yml`

Role Variables
--------------

See `default/main.yml` for an example, for more informations, see Franklinkim's [user role](https://galaxy.ansible.com/franklinkim/users), Gantsign's [Atom.io role](https://galaxy.ansible.com/gantsign/atom/) and [oh-my-zsh role](https://galaxy.ansible.com/gantsign/oh-my-zsh/) variables.

Dependencies
------------

See `meta/main.yml`

Example Playbook
----------------

    - hosts: workstation
      become: yes
      roles:
         - rgarrigue.workstation

As you may want to execute that localy to setup a just installed computer

    ansible-playbook -i 'localhost,' -c local workstation.yml

Tests
-----

This role is tested via molecule, which requires Docker. Provided you got docker installed

    pip install ansible docker molecule    
    molecule test

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue
