#!/bin/bash

echo "Installing requirements"
if [ $(which apt-get) ]
then
  sudo apt-get update
  sudo apt-get install -qq -y python3-pip
else
  echo "This ansible role is meant for Ubuntu, it might work with other apt-based distribution... but apt-get seems to be missing !"
  exit 9000
fi

echo "Installing ansible"
pip3 install ansible

# echo "Installing rgarrigue.workstation and deps from galaxy"
# ansible-galaxy install rgarrigue.workstation

echo "Installing local workstation"
ansible-playbook -i "localhost," -c local playbook.yml
