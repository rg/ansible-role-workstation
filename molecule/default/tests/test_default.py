import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize(
    "path",
    [
        ("/home/remy"),
    ],
)
def test_files_exists(host, path):
    f = host.file(path)
    assert f.exists
    assert f.user == "remy"
    assert f.group == "remy"


@pytest.mark.parametrize(
    "package",
    [
        ("curl"),
        ("git"),
        ("gzip"),
        ("python"),
    ],
)
def test_package_installed(host, package):
    p = host.package(package)
    assert p.is_installed


@pytest.mark.parametrize(
    "service",
    [
        ("haveged"),
    ],
)
def test_service_is_running(host, service):
    s = host.service(service)
    assert s.is_running
    assert s.is_enabled
